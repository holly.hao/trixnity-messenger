package de.connect2x.trixnity.messenger

import org.koin.core.module.Module
import org.koin.dsl.module

actual fun platformEncryptRepositoryModule(): Module = module {
    single<EncryptRepository> {
        EncryptRepository { false }
    }
}