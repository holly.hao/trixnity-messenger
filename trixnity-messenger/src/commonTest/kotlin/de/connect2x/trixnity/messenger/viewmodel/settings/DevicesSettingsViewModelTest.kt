package de.connect2x.trixnity.messenger.viewmodel.settings

import com.arkivanov.decompose.DefaultComponentContext
import com.arkivanov.essenty.lifecycle.LifecycleRegistry
import de.connect2x.trixnity.messenger.firstNotNullWithClue
import de.connect2x.trixnity.messenger.util.AuthorizeUiaMock
import de.connect2x.trixnity.messenger.viewmodel.MatrixClientViewModelContextImpl
import de.connect2x.trixnity.messenger.viewmodel.uia.AuthorizeUia
import de.connect2x.trixnity.messenger.viewmodel.uia.AuthorizeUiaResult
import de.connect2x.trixnity.messenger.viewmodel.util.cancelNeverEndingCoroutines
import de.connect2x.trixnity.messenger.viewmodel.util.createTestDefaultTrixnityMessengerModules
import io.kotest.assertions.assertSoftly
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.core.test.testCoroutineScheduler
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.setMain
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toInstant
import kotlinx.serialization.json.Json
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.client.MatrixClientConfiguration
import net.folivo.trixnity.client.key.DeviceTrustLevel
import net.folivo.trixnity.client.key.KeyService
import net.folivo.trixnity.client.key.KeyTrustService
import net.folivo.trixnity.client.store.KeyStore
import net.folivo.trixnity.client.store.repository.*
import net.folivo.trixnity.client.verification.ActiveDeviceVerificationImpl
import net.folivo.trixnity.client.verification.VerificationService
import net.folivo.trixnity.clientserverapi.client.DeviceApiClient
import net.folivo.trixnity.clientserverapi.client.MatrixClientServerApiClient
import net.folivo.trixnity.clientserverapi.client.UIA
import net.folivo.trixnity.clientserverapi.model.devices.Device
import net.folivo.trixnity.core.model.UserId
import net.folivo.trixnity.core.model.events.m.key.verification.VerificationRequestToDeviceEventContent
import net.folivo.trixnity.core.model.keys.DeviceKeys
import net.folivo.trixnity.core.model.keys.Keys
import net.folivo.trixnity.crypto.olm.OlmDecrypter
import net.folivo.trixnity.crypto.olm.OlmEncryptionService
import org.kodein.mock.Mock
import org.kodein.mock.Mocker
import org.kodein.mock.mockFunction0
import org.koin.dsl.koinApplication
import org.koin.dsl.module
import kotlin.coroutines.CoroutineContext

@OptIn(ExperimentalStdlibApi::class, ExperimentalCoroutinesApi::class)
class DevicesSettingsViewModelTest : ShouldSpec() {
    override fun timeout(): Long = 2_000

    val mocker = Mocker()

    private val ourDeviceId = "deviceId1"
    private val ourDeviceId2 = "deviceId21"
    private val ourUserId = UserId("userId1", "localhost")
    private val ourUserId2 = UserId("userId21", "localhost")

    private val device1 = Device(
        ourDeviceId,
        "device1",
        "1.2.3.4",
        LocalDateTime.parse("2021-12-10T09:50:00.00").toInstant(TimeZone.UTC).toEpochMilliseconds()
    )

    private val device2 = Device(
        "deviceId2",
        "device2",
        "4.3.2.1",
        LocalDateTime.parse("2021-12-10T07:50:00.00").toInstant(TimeZone.UTC).toEpochMilliseconds()
    )

    private val device21 = Device(
        ourDeviceId2,
        "device22",
        "0.9.8.7",
        LocalDateTime.parse("2021-12-10T09:50:00.00").toInstant(TimeZone.UTC).toEpochMilliseconds(),
    )

    private val device22 = Device(
        "deviceId22",
        "device22",
        "7.8.9.0",
        LocalDateTime.parse("2021-12-10T07:50:00.00").toInstant(TimeZone.UTC).toEpochMilliseconds()
    )

    private val deviceKeys = flowOf(
        listOf(DeviceKeys(ourUserId, "deviceId", setOf(), Keys(setOf())))
    )

    private val deviceKeys2 = flowOf(
        listOf(DeviceKeys(ourUserId2, "deviceId", setOf(), Keys(setOf())))
    )

    @Mock
    lateinit var matrixClientMock: MatrixClient

    @Mock
    lateinit var matrixClientMock2: MatrixClient

    @Mock
    lateinit var keyServiceMock: KeyService

    @Mock
    lateinit var keyServiceMock2: KeyService

    @Mock
    lateinit var verificationServiceMock: VerificationService

    @Mock
    lateinit var matrixClientServerApiClientMock: MatrixClientServerApiClient

    @Mock
    lateinit var matrixClientServerApiClientMock2: MatrixClientServerApiClient

    @Mock
    lateinit var devicesApiClientMock: DeviceApiClient

    @Mock
    lateinit var devicesApiClientMock2: DeviceApiClient

    @Mock
    lateinit var olmDecrypterMock: OlmDecrypter

    @Mock
    lateinit var olmEncryptionServiceMock: OlmEncryptionService

    @Mock
    lateinit var keyTrustServiceMock: KeyTrustService

    private lateinit var coroutineScope: CoroutineScope
    private lateinit var authorizeUia: AuthorizeUiaMock

    private lateinit var updateDeviceMocker: Mocker.EverySuspend<Result<Unit>>
    private lateinit var deviceKeysMocker: Mocker.Every<Flow<List<DeviceKeys>?>>

    init {
        coroutineTestScope = true

        beforeTest {
            mocker.reset()
            injectMocks(mocker)

            coroutineScope = CoroutineScope(Dispatchers.Default)
            authorizeUia = AuthorizeUiaMock(coroutineScope)

            with(mocker) {
                every { matrixClientMock.di } returns koinApplication {
                    modules(
                        module {
                            single { keyServiceMock }
                            single { verificationServiceMock }
                        }
                    )
                }.koin
                every { matrixClientMock.deviceId } returns ourDeviceId
                every { matrixClientMock.userId } returns ourUserId
                every { matrixClientMock.api } returns matrixClientServerApiClientMock
                every { matrixClientServerApiClientMock.device } returns devicesApiClientMock
                every { matrixClientServerApiClientMock.json } returns Json

                every { matrixClientMock2.di } returns koinApplication {
                    modules(
                        module {
                            single { keyServiceMock2 }
                            single { verificationServiceMock }
                        }
                    )
                }.koin
                every { matrixClientMock2.deviceId } returns ourDeviceId2
                every { matrixClientMock2.userId } returns ourUserId2
                every { matrixClientMock2.api } returns matrixClientServerApiClientMock2
                every { matrixClientServerApiClientMock2.device } returns devicesApiClientMock2
                every { matrixClientServerApiClientMock2.json } returns Json

                updateDeviceMocker = everySuspending {
                    devicesApiClientMock.updateDevice(isAny(), isAny(), isNull())
                }
                updateDeviceMocker returns Result.success(Unit)

                deviceKeysMocker = every { keyServiceMock.getDeviceKeys(isEqual(ourUserId)) }
                deviceKeysMocker returns deviceKeys
                every { keyServiceMock2.getDeviceKeys(isEqual(ourUserId2)) } returns deviceKeys2
                // standard values for mock2, we do not need any config here
                every {
                    keyServiceMock2.getTrustLevel(isAny<UserId>(), isAny())
                } returns flowOf(DeviceTrustLevel.Valid(true))
                everySuspending { devicesApiClientMock2.getDevices() } returns Result.success(
                    listOf(device21, device22)
                )
                everySuspending {
                    devicesApiClientMock.getDevice(isAny(), isAny())
                } returns Result.success(device1)
            }
        }
        afterTest {
            coroutineScope.cancel()
        }

        should("load devices initially") {
            with(mocker) {
                everySuspending { devicesApiClientMock.getDevices() } returns Result.success(listOf(device1, device2))
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual(ourDeviceId))
                } returns flowOf(DeviceTrustLevel.Valid(true))
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual("deviceId2"))
                } returns flowOf(DeviceTrustLevel.NotCrossSigned)
            }

            val cut = devicesSettingsViewModel(coroutineContext)
            val accountsWithDevices = cut.accountsWithDevices
            testCoroutineScheduler.advanceUntilIdle()
            accountsWithDevices.first { it.size == 2 }

            assertSoftly(accountsWithDevices.value) {
                get(0).userId shouldBe UserId("test", "server")
                get(1).userId shouldBe UserId("test2", "server")

                get(0).isLoading.first { it.not() }
                get(0).devicesInAccount.first { it.thisDevice.deviceId == ourDeviceId }
                assertSoftly(get(0).devicesInAccount.value.thisDevice) {
                    deviceId shouldBe ourDeviceId
                    displayName.value shouldBe "device1"
                    lastSeenAt shouldBe "last seen: 12/10/2021"
                    isVerified.value shouldBe true
                }
                get(0).devicesInAccount.value.otherDevices shouldHaveSize 1
                assertSoftly(get(0).devicesInAccount.value.otherDevices[0]) {
                    deviceId shouldBe "deviceId2"
                    displayName.value shouldBe "device2"
                    lastSeenAt shouldBe "last seen: 12/10/2021"
                    isVerified.value shouldBe false
                }

                get(1).devicesInAccount.first { it.thisDevice.deviceId == ourDeviceId2 }
                get(1).devicesInAccount.value.thisDevice.deviceId shouldBe ourDeviceId2
                get(1).devicesInAccount.value.otherDevices shouldHaveSize 1
            }
            cancelNeverEndingCoroutines()
        }

        should("react to changes in the trust level of devices") {
            val trustLevel1 = MutableStateFlow<DeviceTrustLevel>(DeviceTrustLevel.CrossSigned(true))
            val trustLevel2 = MutableStateFlow<DeviceTrustLevel>(DeviceTrustLevel.NotCrossSigned)
            with(mocker) {
                everySuspending { devicesApiClientMock.getDevices() } returns Result.success(listOf(device1, device2))
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual(ourDeviceId))
                } returns trustLevel1
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual("deviceId2"))
                } returns trustLevel2
            }

            val cut = devicesSettingsViewModel(coroutineContext)
            val accountsWithDevices = cut.accountsWithDevices

            trustLevel1.value = DeviceTrustLevel.CrossSigned(false)
            trustLevel2.value = DeviceTrustLevel.CrossSigned(true)
            testCoroutineScheduler.advanceUntilIdle()

            accountsWithDevices.first { it.size == 2 }

            assertSoftly(accountsWithDevices.value) {
                get(0).devicesInAccount.first { it.thisDevice.deviceId == ourDeviceId }
                assertSoftly(get(0).devicesInAccount.value) {
                    thisDevice.isVerified.value shouldBe false
                    otherDevices[0].isVerified.value shouldBe true
                }
            }

            cancelNeverEndingCoroutines()
        }

        should("react to changes in the devices list") {
            val deviceKeysList = MutableStateFlow(
                listOf(
                    DeviceKeys(
                        userId = ourUserId,
                        deviceId = "deviceId",
                        algorithms = setOf(),
                        keys = Keys(setOf()),
                    )
                )
            )
            deviceKeysMocker returns deviceKeysList

            val getDevicesMocker = mocker.everySuspending { devicesApiClientMock.getDevices() }
            getDevicesMocker returns Result.success(listOf(device1, device2))
            with(mocker) {
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual(ourDeviceId))
                } returns flowOf(DeviceTrustLevel.Valid(true))
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual("deviceId2"))
                } returns flowOf(DeviceTrustLevel.NotCrossSigned)
            }

            val cut = devicesSettingsViewModel(coroutineContext)
            val accountsWithDevices = cut.accountsWithDevices
            testCoroutineScheduler.advanceUntilIdle()

            accountsWithDevices.first {
                it.isNotEmpty() &&
                        it[0].devicesInAccount.value.otherDevices.isNotEmpty() &&
                        it[0].devicesInAccount.value.otherDevices[0].displayName.value == "device2"
            }

            getDevicesMocker returns Result.success(
                listOf(
                    device1,
                    device2.copy(displayName = "device2___new")
                )
            )

            deviceKeysList.value += DeviceKeys(ourUserId, ourDeviceId, setOf(), Keys(setOf()))
            testCoroutineScheduler.advanceUntilIdle()
            accountsWithDevices.first {
                it.isNotEmpty() &&
                        it[0].devicesInAccount.value.otherDevices.isNotEmpty() &&
                        it[0].devicesInAccount.value.otherDevices[0].displayName.value == "device2___new"
            }

            cancelNeverEndingCoroutines()
        }

        should("show an error message if loading devices cannot be performed") {
            mocker.everySuspending { devicesApiClientMock.getDevices() } returns Result.failure(RuntimeException("Oh no!"))

            val cut = devicesSettingsViewModel(coroutineContext)
            val accountsWithDevices = cut.accountsWithDevices
            testCoroutineScheduler.advanceUntilIdle()

            accountsWithDevices.first {
                it.isNotEmpty()
                        && it[0].loadingError.value != null
            }

            cancelNeverEndingCoroutines()
        }

        should("set the display name for this device and update the device") {
            with(mocker) {
                everySuspending { devicesApiClientMock.getDevices() } returns Result.success(listOf(device1, device2))
                every {
                    keyServiceMock.getTrustLevel(isAny<UserId>(), isAny())
                } returns flowOf(DeviceTrustLevel.Valid(true))
            }

            val cut = devicesSettingsViewModel(coroutineContext)
            // wait until initial computation is done
            val accountsWithDevices = cut.accountsWithDevices
            testCoroutineScheduler.advanceUntilIdle()
            accountsWithDevices.first { it.isNotEmpty() && it[0].devicesInAccount.value.thisDevice.deviceId == ourDeviceId }

            cut.setDisplayName(UserId("test", "server"), ourDeviceId, "device1", "device1 updated")
            testCoroutineScheduler.advanceUntilIdle()

            accountsWithDevices.first {
                it.isNotEmpty() &&
                        it[0].devicesInAccount.value.thisDevice.displayName.value == "device1 updated"
            }

            cancelNeverEndingCoroutines()
        }

        should("display error message when device cannot be renamed") {
            with(mocker) {
                updateDeviceMocker returns Result.failure(RuntimeException("Oh no!"))
                everySuspending { devicesApiClientMock.getDevices() } returns Result.success(listOf(device1, device2))
                every {
                    keyServiceMock.getTrustLevel(isAny<UserId>(), isAny())
                } returns flowOf(DeviceTrustLevel.Valid(true))
            }

            val cut = devicesSettingsViewModel(coroutineContext)
            val accountsWithDevices = cut.accountsWithDevices
            testCoroutineScheduler.advanceUntilIdle()
            accountsWithDevices.first { it.isNotEmpty() && it[0].devicesInAccount.value.thisDevice.deviceId == ourDeviceId }

            cut.error.value shouldBe null
            cut.setDisplayName(UserId("test", "server"), ourDeviceId, "device1", "device1 updated")
            testCoroutineScheduler.advanceUntilIdle()

            cut.error.value shouldNotBe null

            cancelNeverEndingCoroutines()
        }

        should("initiate a verification request") {
            with(mocker) {
                everySuspending {
                    verificationServiceMock.createDeviceVerificationRequest(isEqual(ourUserId), isAny())
                } returns Result.success(activeDeviceVerification(CoroutineScope(testCoroutineScheduler)))
                everySuspending { devicesApiClientMock.getDevices() } returns Result.success(listOf(device1, device2))
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual(ourDeviceId))
                } returns flowOf(DeviceTrustLevel.Valid(true))
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual("deviceId2"))
                } returns flowOf(DeviceTrustLevel.NotCrossSigned)
            }

            val cut = devicesSettingsViewModel(coroutineContext)
            val accountsWithDevices = cut.accountsWithDevices
            accountsWithDevices.first { it.isNotEmpty() && it[0].devicesInAccount.value.thisDevice.deviceId == ourDeviceId }

            cut.verify(UserId("test", "server"), "deviceId2")
            testCoroutineScheduler.advanceUntilIdle()

            mocker.verifyWithSuspend(exhaustive = false) {
                verificationServiceMock.createDeviceVerificationRequest(
                    isEqual(ourUserId),
                    isEqual(setOf("deviceId2")),
                )
            }

            cancelNeverEndingCoroutines()
        }

        should("show an error message when verification cannot be performed") {
            with(mocker) {
                everySuspending {
                    verificationServiceMock.createDeviceVerificationRequest(isEqual(ourUserId), isAny())
                } runs { throw RuntimeException("Oh no!") }
                everySuspending { devicesApiClientMock.getDevices() } returns Result.success(listOf(device1, device2))
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual(ourDeviceId))
                } returns flowOf(DeviceTrustLevel.Valid(true))
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual("deviceId2"))
                } returns flowOf(DeviceTrustLevel.NotCrossSigned)
            }

            val cut = devicesSettingsViewModel(coroutineContext)
            val accountsWithDevices = cut.accountsWithDevices
            accountsWithDevices.first { it.isNotEmpty() && it[0].devicesInAccount.value.thisDevice.deviceId == ourDeviceId }
            cut.error.value shouldBe null
            cut.verify(UserId("test", "server"), "deviceId2")
            testCoroutineScheduler.advanceUntilIdle()

            cut.error.value shouldNotBe null

            cancelNeverEndingCoroutines()
        }

        should("show uia when trying to remove a device") {
            with(mocker) {
                everySuspending { devicesApiClientMock.getDevices() } returns Result.success(listOf(device1, device2))
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual(ourDeviceId))
                } returns flowOf(DeviceTrustLevel.Valid(true))
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual("deviceId2"))
                } returns flowOf(DeviceTrustLevel.NotCrossSigned)
            }

            val cut = devicesSettingsViewModel(coroutineContext)
            val accountsWithDevices = cut.accountsWithDevices
            accountsWithDevices.first { it.isNotEmpty() && it[0].devicesInAccount.value.thisDevice.deviceId == ourDeviceId }

            cut.remove(UserId("test", "server"), "deviceId2")
            val authorizeUiaParams = authorizeUia.onRequestFlowState.firstNotNullWithClue()
            authorizeUiaParams.onResult(AuthorizeUiaResult.Success(UIA.Success(Unit)))

            cancelNeverEndingCoroutines()
        }

        should("show an error message when the device could not be removed") {
            with(mocker) {
                everySuspending { devicesApiClientMock.getDevices() } returns Result.success(listOf(device1, device2))
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual(ourDeviceId))
                } returns flowOf(DeviceTrustLevel.Valid(true))
                every {
                    keyServiceMock.getTrustLevel(isEqual(ourUserId), isEqual("deviceId2"))
                } returns flowOf(DeviceTrustLevel.NotCrossSigned)
            }

            val cut = devicesSettingsViewModel(coroutineContext)
            val accountsWithDevices = cut.accountsWithDevices
            accountsWithDevices.first { it.isNotEmpty() && it[0].devicesInAccount.value.thisDevice.deviceId == ourDeviceId }

            cut.error.value shouldBe null
            cut.remove(UserId("test", "server"), "deviceId2")
            val authorizeUiaParams = authorizeUia.onRequestFlowState.firstNotNullWithClue()
            authorizeUiaParams.onResult(AuthorizeUiaResult.CancelledByUser<Unit>("cancelled"))

            cut.error.firstNotNullWithClue()

            cancelNeverEndingCoroutines()
        }
    }

    private fun activeDeviceVerification(scope: CoroutineScope) = ActiveDeviceVerificationImpl(
        request = VerificationRequestToDeviceEventContent("", emptySet(), 0L, ""),
        requestIsOurs = false,
        ownUserId = ourUserId,
        ownDeviceId = ourDeviceId,
        theirDeviceId = "",
        theirUserId = UserId(""),
        theirDeviceIds = emptySet(),
        supportedMethods = emptySet(),
        api = matrixClientServerApiClientMock,
        olmDecrypter = olmDecrypterMock,
        olmEncryptionService = olmEncryptionServiceMock,
        keyTrust = keyTrustServiceMock,
        keyStore = KeyStore(
            outdatedKeysRepository = InMemoryOutdatedKeysRepository(),
            deviceKeysRepository = InMemoryDeviceKeysRepository(),
            crossSigningKeysRepository = InMemoryCrossSigningKeysRepository(),
            keyVerificationStateRepository = InMemoryKeyVerificationStateRepository(),
            keyChainLinkRepository = InMemoryKeyChainLinkRepository(),
            secretsRepository = InMemorySecretsRepository(),
            secretKeyRequestRepository = InMemorySecretKeyRequestRepository(),
            roomKeyRequestRepository = InMemoryRoomKeyRequestRepository(),
            tm = NoOpRepositoryTransactionManager,
            config = MatrixClientConfiguration(),
            storeScope = scope,
        ),
    )

    private suspend fun devicesSettingsViewModel(coroutineContext: CoroutineContext): DevicesSettingsViewModelImpl {
        Dispatchers.setMain(checkNotNull(currentCoroutineContext()[CoroutineDispatcher]))
        val di = koinApplication {
            modules(
                createTestDefaultTrixnityMessengerModules(
                    mapOf(
                        UserId("test", "server") to matrixClientMock,
                        UserId("test2", "server") to matrixClientMock2
                    )
                ) + module {
                    single<AuthorizeUia> { authorizeUia }
                }
            )
        }.koin
        return DevicesSettingsViewModelImpl(
            viewModelContext = MatrixClientViewModelContextImpl(
                componentContext = DefaultComponentContext(LifecycleRegistry()),
                di = di,
                userId = UserId("test", "server"),
                coroutineContext = coroutineContext
            ),
            mockFunction0(mocker),
        )
    }
}
