package de.connect2x.trixnity.messenger.viewmodel.util

object RegexUtil {
    val matchBlankSpace by lazy { "\\s".toRegex() }
}
