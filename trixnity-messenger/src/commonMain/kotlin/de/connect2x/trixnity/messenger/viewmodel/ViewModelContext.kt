package de.connect2x.trixnity.messenger.viewmodel

import com.arkivanov.decompose.ComponentContext
import com.arkivanov.decompose.childContext
import com.arkivanov.essenty.lifecycle.Lifecycle
import de.connect2x.trixnity.messenger.MatrixClients
import de.connect2x.trixnity.messenger.i18n.I18n
import de.connect2x.trixnity.messenger.viewmodel.util.coroutineScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import net.folivo.trixnity.client.MatrixClient
import net.folivo.trixnity.core.model.UserId
import org.koin.core.Koin
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import kotlin.coroutines.CoroutineContext


interface ViewModelContext : KoinComponent, ComponentContext {
    /**
     * This should be used carefully, because it can lead to leaks when not used on the top level.
     */
    val coroutineScope: CoroutineScope
    fun childContext(key: String, lifecycle: Lifecycle? = null): ViewModelContext
    fun childContext(componentContext: ComponentContext): ViewModelContext
    fun childContext(key: String, lifecycle: Lifecycle? = null, userId: UserId): MatrixClientViewModelContext

    fun childContext(componentContext: ComponentContext, userId: UserId): MatrixClientViewModelContext
}

interface MatrixClientViewModelContext : ViewModelContext {
    val matrixClient: MatrixClient
    val userId: UserId

    override fun childContext(key: String, lifecycle: Lifecycle?): MatrixClientViewModelContext
    override fun childContext(componentContext: ComponentContext): MatrixClientViewModelContext
}

val ViewModelContext.i18n: I18n
    get() = get<I18n>()

val ViewModelContext.matrixClients: MatrixClients
    get() = get<MatrixClients>()

fun ViewModelContext.getMatrixClient(userId: UserId) =
    checkNotNull(get<MatrixClients>().value[userId]) { "cannot find MatrixClient for $userId" }

open class ViewModelContextImpl(
    private val di: Koin,
    componentContext: ComponentContext,
    protected val coroutineContext: CoroutineContext = Dispatchers.Default,
) : ViewModelContext,
    ComponentContext by componentContext {
    final override val coroutineScope: CoroutineScope = componentContext.coroutineScope(coroutineContext)

    override fun getKoin(): Koin = di

    override fun childContext(key: String, lifecycle: Lifecycle?): ViewModelContext {
        val componentContext = this as ComponentContext
        return childContext(componentContext.childContext(key, lifecycle))
    }

    override fun childContext(componentContext: ComponentContext): ViewModelContext {
        return ViewModelContextImpl(
            getKoin(),
            componentContext,
            coroutineContext
        )
    }

    override fun childContext(key: String, lifecycle: Lifecycle?, userId: UserId): MatrixClientViewModelContext {
        val componentContext = this as ComponentContext
        return childContext(componentContext.childContext(key, lifecycle), userId)
    }

    override fun childContext(componentContext: ComponentContext, userId: UserId): MatrixClientViewModelContext {
        return MatrixClientViewModelContextImpl(
            getKoin(),
            componentContext,
            userId,
            coroutineContext
        )
    }
}

open class MatrixClientViewModelContextImpl(
    di: Koin,
    componentContext: ComponentContext,
    override val userId: UserId,
    coroutineContext: CoroutineContext = Dispatchers.Default,
) : MatrixClientViewModelContext, ViewModelContextImpl(di, componentContext, coroutineContext) {
    override val matrixClient by lazy { getMatrixClient(userId) }

    override fun childContext(key: String, lifecycle: Lifecycle?): MatrixClientViewModelContext {
        val componentContext = this as ComponentContext
        return childContext(componentContext.childContext(key, lifecycle))
    }

    override fun childContext(componentContext: ComponentContext): MatrixClientViewModelContext {
        return MatrixClientViewModelContextImpl(
            getKoin(),
            componentContext,
            userId,
            coroutineContext
        )
    }
}