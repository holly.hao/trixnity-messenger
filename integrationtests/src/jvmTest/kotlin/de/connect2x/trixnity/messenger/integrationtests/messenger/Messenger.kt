package de.connect2x.trixnity.messenger.integrationtests.messenger

import de.connect2x.trixnity.messenger.MatrixClients
import de.connect2x.trixnity.messenger.MatrixMessenger
import de.connect2x.trixnity.messenger.createRoot
import de.connect2x.trixnity.messenger.integrationtests.util.waitFor
import de.connect2x.trixnity.messenger.viewmodel.MainViewModel
import de.connect2x.trixnity.messenger.viewmodel.RootRouter
import de.connect2x.trixnity.messenger.viewmodel.RootViewModel
import de.connect2x.trixnity.messenger.viewmodel.connecting.AddMatrixAccountMethod
import de.connect2x.trixnity.messenger.viewmodel.connecting.AddMatrixAccountViewModel
import de.connect2x.trixnity.messenger.viewmodel.initialsync.InitialSyncRouter
import de.connect2x.trixnity.messenger.viewmodel.room.RoomRouter
import de.connect2x.trixnity.messenger.viewmodel.room.timeline.TimelineRouter
import de.connect2x.trixnity.messenger.viewmodel.roomlist.RoomListRouter
import de.connect2x.trixnity.messenger.viewmodel.settings.AccountsOverviewViewModel
import de.connect2x.trixnity.messenger.viewmodel.uia.UiaRouter
import de.connect2x.trixnity.messenger.viewmodel.util.toFlow
import de.connect2x.trixnity.messenger.viewmodel.verification.SelfVerificationRouter
import de.connect2x.trixnity.messenger.viewmodel.verification.VerificationRouter
import de.connect2x.trixnity.messenger.viewmodel.verification.VerificationViewModel
import io.github.oshai.kotlinlogging.KotlinLogging
import io.kotest.assertions.nondeterministic.eventually
import io.kotest.matchers.equals.shouldBeEqual
import io.kotest.matchers.shouldBe
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.withTimeout
import net.folivo.trixnity.client.verification.SelfVerificationMethod
import net.folivo.trixnity.core.model.RoomId
import kotlin.time.Duration.Companion.seconds

private val log = KotlinLogging.logger { }

class MatrixMessengerWithRoot(
    delegate: MatrixMessenger,
    val root: RootViewModel = delegate.createRoot()
) : MatrixMessenger by delegate

suspend fun MatrixMessengerWithRoot.login(
    serverUrl: String,
    username: String,
    password: String,
    recoveryKey: String? = null,
    otherMessenger: MatrixMessengerWithRoot? = null,
): String? = with(root) {
    log.debug { " +- ADD ACCOUNT" }
    addMatrixAccountViaPassword(serverUrl, username, password)
    log.debug { " +- try login" }
    val main = stack.waitFor(RootRouter.Wrapper.Main::class)
    log.info { " +- main view" }
    val mainViewModel = main.viewModel
    val verification = mainViewModel.selfVerificationStack.toFlow().first { childStack ->
        log.debug { " active: ${childStack.active.instance}" }
        childStack.active.instance is SelfVerificationRouter.Wrapper.Bootstrap ||
                childStack.active.instance is SelfVerificationRouter.Wrapper.View
    }.active.instance
    if (verification is SelfVerificationRouter.Wrapper.View) {
        if (recoveryKey != null) {
            selfVerify(verification, recoveryKey)
            mainViewModel.selfVerificationStack.waitFor(SelfVerificationRouter.Wrapper.None::class)
            log.info { "self verification done successfully" }
        } else {
            if (otherMessenger != null) {
                selfVerify(verification, mainViewModel, otherMessenger.root)
            } else {
                log.error { "cannot self verify without recovery key or other device" }
                throw IllegalStateException("cannot self verify without recovery key or other device")
            }
        }
        return recoveryKey
    } else {
        return bootstrap(verification, username, password)
    }
}

suspend fun MatrixMessengerWithRoot.createNewAccount(
    serverUrl: String,
    username: String,
    password: String,
    recoveryKey: String? = null,
): String? = with(root) {
    val accountsOverviewViewModel = openAccountsOverview()
    accountsOverviewViewModel.createNewAccount()
    val thisRecoveryKey = login(serverUrl, username, password, recoveryKey)
    val mainViewModel = stack.waitFor(RootRouter.Wrapper.Main::class).viewModel
    mainViewModel.roomListRouterStack.waitFor(RoomListRouter.Wrapper.List::class)
    mainViewModel.initialSyncStack.waitFor(InitialSyncRouter.Wrapper.None::class)
    return thisRecoveryKey ?: recoveryKey
}

suspend fun MatrixMessengerWithRoot.deleteAccount(username: String) = with(root) {
    val accountsOverviewViewModel = openAccountsOverview()
    val userId = di.get<MatrixClients>().value.keys.find { it.localpart == username }
    checkNotNull(userId)
    accountsOverviewViewModel.removeAccount(userId)
    stack.waitFor(RootRouter.Wrapper.MatrixClientLogout::class)
    val mainViewModel = stack.waitFor(RootRouter.Wrapper.Main::class).viewModel
    mainViewModel.roomListRouterStack.waitFor(RoomListRouter.Wrapper.List::class)
    mainViewModel.initialSyncStack.waitFor(InitialSyncRouter.Wrapper.None::class)
    log.debug { " +- delete account finished" }
}

suspend fun MatrixMessengerWithRoot.verifyAccountsArePresent(vararg usernames: String) = with(root) {
    val accountsOverviewViewModel = openAccountsOverview()
    withTimeout(5.seconds) {
        eventually(4.seconds) {
            accountsOverviewViewModel.accounts
                .map { accounts -> accounts.map { it.userId.localpart } }
                .first { it.containsAll(usernames.toList()) }
        }
        accountsOverviewViewModel.close()
        stack.waitFor(RootRouter.Wrapper.Main::class).viewModel
            .roomListRouterStack.waitFor(RoomListRouter.Wrapper.List::class)
    }
}

suspend fun MatrixMessengerWithRoot.registerAccountWithToken(serverUrl: String, registrationToken: String) =
    with(root) {
        withTimeout(10.seconds) {
            val addMatrixAccountViewModel =
                stack.waitFor(RootRouter.Wrapper.AddMatrixAccount::class).viewModel
            addMatrixAccountViewModel.serverUrl.value = serverUrl
            val registerMethod = addMatrixAccountViewModel.serverDiscoveryState
                .filterIsInstance<AddMatrixAccountViewModel.ServerDiscoveryState.Success>().first()
                .addMatrixAccountMethods.filterIsInstance<AddMatrixAccountMethod.Register>().first()
            addMatrixAccountViewModel.selectAddMatrixAccountMethod(registerMethod)
            val registerNewAccountViewModel =
                stack.waitFor(RootRouter.Wrapper.RegisterNewAccount::class).viewModel
            registerNewAccountViewModel.username.update { "user1" }
            registerNewAccountViewModel.password.update { "user1password" }
            registerNewAccountViewModel.canRegisterNewUser.first { it }
            registerNewAccountViewModel.register()
            authorizeUia(registrationToken)

            val mainViewModel = stack.waitFor(RootRouter.Wrapper.Main::class).viewModel
            mainViewModel.roomListRouterStack.waitFor(RoomListRouter.Wrapper.List::class).viewModel.accountViewModel.accounts.first { it.isNotEmpty() }
            mainViewModel.initialSyncStack.waitFor(InitialSyncRouter.Wrapper.None::class)
        }
    }

@OptIn(ExperimentalCoroutinesApi::class)
suspend fun MatrixMessengerWithRoot.createChatWithUser(username: String) = with(root) {
    withTimeout(15.seconds) {
        log.info { "create a chat with user '$username'" }
        val roomListRouterStack = stack.waitFor(RootRouter.Wrapper.Main::class).viewModel.roomListRouterStack
        val roomListViewModel = roomListRouterStack.waitFor(RoomListRouter.Wrapper.List::class).viewModel
        roomListViewModel.createNewRoom()
        val createNewChatViewModel = roomListRouterStack.waitFor(RoomListRouter.Wrapper.CreateNewChat::class).viewModel
        log.debug { "search for user '$username'" }
        createNewChatViewModel.createNewRoomViewModel.userSearchTerm.update { username }
        createNewChatViewModel.createNewRoomViewModel.waitForUserResults.first { it.not() }
        val users =
            createNewChatViewModel.createNewRoomViewModel.foundUsers.first { users -> users.any { it.displayName == username } }
        createNewChatViewModel.onUserClick(users.first())
        log.debug { "chat should have been created -> check to find it in the list" }
        val sortedRoomListElementViewModels = roomListRouterStack.waitFor(RoomListRouter.Wrapper.List::class)
            .viewModel.sortedRoomListElementViewModels
        sortedRoomListElementViewModels.flatMapLatest { roomListElements ->
            combine(roomListElements.map { it.viewModel.roomName }) { roomNames ->
                log.debug { "roomNames: ${roomNames.joinToString { it ?: "<unknown>" }}" }
                roomNames.any { it == username }
            }
        }.first { it }
        log.debug { "found room -> return" }
        sortedRoomListElementViewModels.value.first { it.viewModel.roomName.value == username }
    }
}

@OptIn(ExperimentalCoroutinesApi::class)
suspend fun MatrixMessengerWithRoot.createGroupWithUsers(groupName: String, vararg usernames: String) = with(root) {
    withTimeout(20.seconds) {
        log.info { "create a group '$groupName' with users '${usernames.joinToString { it }}'" }
        val roomListRouterStack = stack.waitFor(RootRouter.Wrapper.Main::class).viewModel.roomListRouterStack
        val roomListViewModel = roomListRouterStack.waitFor(RoomListRouter.Wrapper.List::class).viewModel
        roomListViewModel.createNewRoom()
        roomListRouterStack.waitFor(RoomListRouter.Wrapper.CreateNewChat::class).viewModel.createGroup()
        val createNewGroupViewModel =
            roomListRouterStack.waitFor(RoomListRouter.Wrapper.CreateNewGroup::class).viewModel
        usernames.forEach { username ->
            log.debug { "search for user '$username'" }
            createNewGroupViewModel.createNewRoomViewModel.userSearchTerm.update { username }
            createNewGroupViewModel.createNewRoomViewModel.waitForUserResults.first { it.not() }
            val users =
                createNewGroupViewModel.createNewRoomViewModel.foundUsers.first { users -> users.any { it.displayName == username } }
            createNewGroupViewModel.onUserClick(users.first())
        }
        createNewGroupViewModel.optionalRoomName.update { groupName }
        createNewGroupViewModel.createNewGroup()
        log.debug { "group '$groupName' should have been created -> check to find it in the list" }
        val sortedRoomListElementViewModels = roomListRouterStack.waitFor(RoomListRouter.Wrapper.List::class)
            .viewModel.sortedRoomListElementViewModels
        sortedRoomListElementViewModels.flatMapLatest { roomListElements ->
            combine(roomListElements.map { it.viewModel.roomName }) { roomNames ->
                log.debug { "roomNames: ${roomNames.joinToString { it ?: "<unknown>" }}" }
                roomNames.any { it == groupName }
            }
        }.first { it }
        log.debug { "found group -> return" }
        sortedRoomListElementViewModels.value.first { it.viewModel.roomName.value == groupName }
    }
}

suspend fun MatrixMessengerWithRoot.rejectTheInvitationToRoomAndBlock(roomId: RoomId) = with(root) {
    withTimeout(10.seconds) {
        log.info { "reject the invitation at $roomId" }
        log.debug { "found room $roomId, now reject the invitation" }
        findRoomWithId(roomId).viewModel.rejectInvitationAndBlockInviter()
        stack.waitFor(RootRouter.Wrapper.Main::class).viewModel
            .roomListRouterStack.waitFor(RoomListRouter.Wrapper.List::class).viewModel
            .sortedRoomListElementViewModels.first { it.none { it.roomId == roomId } }
        Unit
    }
}

suspend fun MatrixMessengerWithRoot.acceptInvitationToRoom(roomId: RoomId) = with(root) {
    withTimeout(10.seconds) {
        log.info { "accept the invitation to room $roomId" }
        val roomListElementViewModel = findRoomWithId(roomId).viewModel
        roomListElementViewModel.isInvite.first { it ?: false }
        roomListElementViewModel.acceptInvitation()
        val roomName = roomListElementViewModel.roomName.first {
            it?.startsWith("invitation")?.not() ?: false
        }
        log.info { "accepted invitation to room $roomId -> check whether room is open" }
        val timelineViewModel = stack.waitFor(RootRouter.Wrapper.Main::class).viewModel
            .roomRouterStack.waitFor(RoomRouter.Wrapper.View::class).viewModel
            .timelineStack.waitFor(TimelineRouter.Wrapper.View::class).viewModel
        timelineViewModel.roomHeaderViewModel.roomHeaderInfo.map { it.roomName }.first { it == roomName }
    }
}

suspend fun MatrixMessengerWithRoot.leaveRoom(roomId: RoomId) = with(root) {
    withTimeout(15.seconds) {
        log.info { "leave room $roomId" }
        val roomName = findRoomWithId(roomId).viewModel.roomName.first { it != null }
        val mainViewModel = stack.waitFor(RootRouter.Wrapper.Main::class).viewModel
        val roomListViewModel = mainViewModel
            .roomListRouterStack.waitFor(RoomListRouter.Wrapper.List::class).viewModel
        roomListViewModel.selectRoom(roomId)
        val timelineViewModel = mainViewModel
            .roomRouterStack.waitFor(RoomRouter.Wrapper.View::class).viewModel
            .timelineStack.waitFor(TimelineRouter.Wrapper.View::class).viewModel
        timelineViewModel.roomHeaderViewModel.roomHeaderInfo.map { it.roomName }.first { it == roomName }
        timelineViewModel.leaveRoom()
        log.debug { "left room $roomId" }
        mainViewModel.roomRouterStack.waitFor(RoomRouter.Wrapper.None::class)
        roomListViewModel.sortedRoomListElementViewModels.first { roomListElements ->
            roomListElements.none { it.roomId == roomId }
        }
        log.debug { "left room is no longer in room list" }
    }
}

suspend fun MatrixMessengerWithRoot.findRoomWithId(roomId: RoomId) = with(root) {
    withTimeout(10.seconds) {
        log.info { "try to find the room $roomId" }
        val mainViewModel = stack.waitFor(RootRouter.Wrapper.Main::class).viewModel
        val roomListRouterStack = mainViewModel.roomListRouterStack
        val roomListElements = roomListRouterStack.waitFor(RoomListRouter.Wrapper.List::class)
            .viewModel.sortedRoomListElementViewModels.first { roomListElements ->
                log.debug { "found ${roomListElements.size} rooms" }
                roomListElements.any {
                    log.trace { "found ${it.roomId}" }
                    it.roomId == roomId
                }
            }
        roomListElements.first()
    }
}

private suspend fun RootViewModel.openAccountsOverview(): AccountsOverviewViewModel {
    val mainViewModel = stack.waitFor(RootRouter.Wrapper.Main::class).viewModel
    val roomListViewModel =
        mainViewModel.roomListRouterStack.waitFor(RoomListRouter.Wrapper.List::class).viewModel
    roomListViewModel.openAccountsOverview()
    return mainViewModel.roomListRouterStack.waitFor(RoomListRouter.Wrapper.AccountsOverview::class).viewModel
}

private suspend fun RootViewModel.addMatrixAccountViaPassword(
    serverUrl: String,
    username: String,
    password: String,
) {
    val addMatrixAccount = stack.waitFor(RootRouter.Wrapper.AddMatrixAccount::class)
    val addMatrixAccountViewModel = addMatrixAccount.viewModel
    addMatrixAccountViewModel.serverUrl.value = serverUrl
    val registerMethod = addMatrixAccountViewModel.serverDiscoveryState
        .filterIsInstance<AddMatrixAccountViewModel.ServerDiscoveryState.Success>().first()
        .addMatrixAccountMethods.filterIsInstance<AddMatrixAccountMethod.Password>().first()
    addMatrixAccountViewModel.selectAddMatrixAccountMethod(registerMethod)
    val passwordLoginViewModel =
        stack.waitFor(RootRouter.Wrapper.PasswordLogin::class).viewModel
    addMatrixAccountViewModel.serverUrl.value = serverUrl
    passwordLoginViewModel.username.value = username
    passwordLoginViewModel.password.value = password
    passwordLoginViewModel.canLogin.first { it }
    passwordLoginViewModel.tryLogin()
}

private suspend fun MatrixMessengerWithRoot.bootstrap(
    verification: SelfVerificationRouter.Wrapper,
    username: String,
    password: String,
): String? {
    log.info { "  +- bootstrap" }
    val bootstrapViewModel =
        (verification as SelfVerificationRouter.Wrapper.Bootstrap).viewModel
    bootstrapViewModel.bootstrap()

    authorizeUia(username, password)

    bootstrapViewModel.isBootstrapRunning.first { it.not() }
    val createdRecoveryKey = bootstrapViewModel.recoveryKey.first { it != null }
    log.info { "user '$username' with password '$password' has recovery key '$createdRecoveryKey'" }

    bootstrapViewModel.close()
    log.info { "   - bootstrap finished" }
    return createdRecoveryKey
}

private suspend fun selfVerify(
    verification: SelfVerificationRouter.Wrapper,
    recoveryKey: String,
) {
    log.info { "  +- self verification with recovery key" }
    val selfVerificationViewModel =
        (verification as SelfVerificationRouter.Wrapper.View).viewModel
    selfVerificationViewModel.waitForAvailableVerificationMethods()
    selfVerificationViewModel.selfVerificationMethods.first { it.any { selfVerificationMethod -> selfVerificationMethod is SelfVerificationMethod.AesHmacSha2RecoveryKey } }
    selfVerificationViewModel.launchVerification(
        selfVerificationViewModel.selfVerificationMethods.value.find {
            it is SelfVerificationMethod.AesHmacSha2RecoveryKey
        } ?: throw IllegalStateException("Can only use recovery key method"))
    selfVerificationViewModel.verifyWithRecoveryKey(recoveryKey)
}

private suspend fun selfVerify(
    verification: SelfVerificationRouter.Wrapper,
    mainViewModel: MainViewModel,
    otherMessenger: RootViewModel,
) {
    log.info { "  +- self verification with other device" }
    val selfVerificationViewModel =
        (verification as SelfVerificationRouter.Wrapper.View).viewModel
    selfVerificationViewModel.waitForAvailableVerificationMethods()
    selfVerificationViewModel.selfVerificationMethods.first { it.any { selfVerificationMethod -> selfVerificationMethod is SelfVerificationMethod.CrossSignedDeviceVerification } }
    selfVerificationViewModel.launchVerification(selfVerificationViewModel.selfVerificationMethods.value.find {
        it is SelfVerificationMethod.CrossSignedDeviceVerification
    } ?: throw IllegalStateException("can only use device verification"))

    mainViewModel.selfVerificationStack.waitFor(SelfVerificationRouter.Wrapper.None::class)
    val verificationViewModel =
        mainViewModel.deviceVerificationRouterStack.waitFor(VerificationRouter.Wrapper.Verification::class).viewModel
    verificationViewModel.stack.waitFor(VerificationViewModel.Wrapper.Wait::class)

    val otherMainViewModel = otherMessenger.stack.waitFor(RootRouter.Wrapper.Main::class).viewModel
    val otherVerificationViewModel =
        otherMainViewModel.deviceVerificationRouterStack.waitFor(VerificationRouter.Wrapper.Verification::class).viewModel
    val otherVerificationStepRequestViewModel =
        otherVerificationViewModel.stack.waitFor(VerificationViewModel.Wrapper.Request::class).viewModel
    otherVerificationStepRequestViewModel.next()

    val selectVerificationMethodViewModel =
        verificationViewModel.stack.waitFor(VerificationViewModel.Wrapper.SelectVerificationMethod::class).viewModel
    selectVerificationMethodViewModel.verificationMethods.size shouldBe 1
    selectVerificationMethodViewModel.acceptVerificationMethod(selectVerificationMethodViewModel.verificationMethods[0].first)

    val otherAcceptSasStartViewModel =
        otherVerificationViewModel.stack.waitFor(VerificationViewModel.Wrapper.AcceptSasStart::class).viewModel
    otherAcceptSasStartViewModel.accept()

    val verificationStepCompareViewModel =
        verificationViewModel.stack.waitFor(VerificationViewModel.Wrapper.CompareEmojisOrNumbers::class).viewModel
    val otherVerificationStepCompareViewModel =
        otherVerificationViewModel.stack.waitFor(VerificationViewModel.Wrapper.CompareEmojisOrNumbers::class).viewModel
    verificationStepCompareViewModel.emojis shouldBeEqual otherVerificationStepCompareViewModel.emojis

    verificationStepCompareViewModel.accept()
    otherVerificationStepCompareViewModel.accept()

    val verificationStepSuccessViewModel =
        verificationViewModel.stack.waitFor(VerificationViewModel.Wrapper.Success::class).viewModel
    val otherVerificationStepSuccessViewModel =
        otherVerificationViewModel.stack.waitFor(VerificationViewModel.Wrapper.Success::class).viewModel
    verificationStepSuccessViewModel.ok()
    otherVerificationStepSuccessViewModel.ok()

    mainViewModel.deviceVerificationRouterStack.toFlow()
        .first { it.active.configuration is VerificationRouter.Config.None }
    otherMainViewModel.deviceVerificationRouterStack.toFlow()
        .first { it.active.configuration is VerificationRouter.Config.None }
}

suspend fun MatrixMessengerWithRoot.authorizeUia(username: String, password: String) = with(root) {
    uiaStack.waitFor(UiaRouter.Wrapper.UiaActionConfirmation::class).viewModel.next()
    val uiaStepPasswordViewModel = uiaStack.waitFor(UiaRouter.Wrapper.UiaStepPassword::class).viewModel
    uiaStepPasswordViewModel.username.value = username
    uiaStepPasswordViewModel.password.value = password
    uiaStepPasswordViewModel.submit()
    uiaStack.waitFor(UiaRouter.Wrapper.None::class)
}

suspend fun MatrixMessengerWithRoot.authorizeUia(registrationToken: String) = with(root) {
    uiaStack.waitFor(UiaRouter.Wrapper.UiaActionConfirmation::class).viewModel.next()
    val uiaRegistrationTokenViewModel =
        uiaStack.waitFor(UiaRouter.Wrapper.UiaStepRegistrationToken::class).viewModel
    uiaRegistrationTokenViewModel.registrationToken.value = registrationToken
    uiaRegistrationTokenViewModel.submit()
    uiaStack.waitFor(UiaRouter.Wrapper.None::class)
}

suspend fun MatrixMessengerWithRoot.authorizeUia() = with(root) {
    uiaStack.waitFor(UiaRouter.Wrapper.UiaActionConfirmation::class).viewModel.next()
    val uiaStepDummyViewModel = uiaStack.waitFor(UiaRouter.Wrapper.UiaStepDummy::class).viewModel
    uiaStepDummyViewModel.next()
    uiaStack.waitFor(UiaRouter.Wrapper.None::class)
}
